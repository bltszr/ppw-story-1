from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.

class story9Test(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story9:index'))
        self.assertEquals(200, response.status_code)
    
    def test_GET_register(self):
        response = self.client.get(reverse('story9:register'))
        self.assertEquals(200, response.status_code)

    def test_GET_login(self):
        response = self.client.get(reverse('story9:login'))
        self.assertEquals(200, response.status_code)
    
    def test_POST_login_fail(self):
        response = self.client.post(reverse('story9:login'), {"this_is": "a_bad_response"})
        self.assertEquals(200, response.status_code)
        self.assertContains(response, 'Login data not valid.', html=True)
        
    def test_POST_register_fail(self):
        response = self.client.post(reverse('story9:register'), {"this_is": "a_lso_a_bad_response"})
        self.assertEquals(200, response.status_code)
        self.assertContains(response, 'Please fill all forms correctly.', html=True)
    
    def test_POST_login_and_register_success(self):
        response = self.client.post(reverse('story9:register'), {
            "username": "hellooooooooooooooo",
            "password1": "KYcZr53P6ysHupf",
            "password2": "KYcZr53P6ysHupf"
        })
        self.assertEquals(302, response.status_code)
        response = self.client.post(reverse('story9:login'), {
            "username": "hellooooooooooooooo",
            "password": "KYcZr53P6ysHupf"
        })
        self.assertEquals(302, response.status_code)
        response = self.client.get(reverse('story9:login'))
        self.assertEquals(302, response.status_code)
        response = self.client.get(reverse('story9:logoff'))
        self.assertEquals(302, response.status_code)