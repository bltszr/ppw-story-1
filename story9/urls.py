from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('login/', views.logon, name='login'),
    path('logoff/', views.logoff, name='logoff')
]
