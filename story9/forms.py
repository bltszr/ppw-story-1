from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

form_class_str = "form-control"
class RegistrationForm(UserCreationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        "class" : form_class_str,
        "placeholder" : "Username (min. 9 characters)" # I wanted to write this as 君の名は so badly
    }))

    password1 = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        "class" : form_class_str,
        "placeholder" : "Password (min. 8 characters)",
        "type" : "password"
    }))

    password2 = forms.CharField(label="Confirm Password", widget=forms.TextInput(attrs={
        "class" : form_class_str,
        "placeholder" : "Confirm your password",
        "type" : "password"
    }))

    class Meta:
        model = User
        fields = ["username", "password1", "password2"]

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        "class" : form_class_str,
        "placeholder" : "Username"
    }))

    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        "class" : form_class_str,
        "placeholder" : "Password",
        "type" : "password"
    }))

    class Meta:
        fields = "__all__"
