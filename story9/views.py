# ideas/boilerplate/basically this entire story taken/inspired from Matthew T.P.'s work
# on https://gitlab.com/matthewsolomon_/story6/-/blob/master/story9. Thanks, Matt!
from django.shortcuts import render, redirect
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm
from random import randint
# Create your views here.
    
list_of_hellos = [
    'halo, {}!',
    'hello, {}!',
    'hallo, {}!',
    'おっす、 {}!',
    'привет, {}!',
    '你好, {}!',
    '哈囉, {}!',
    'xin chào, {}!',
    'salut, {}!',
    '안녕, {}!',
    'goddag, {}!',
    'olá, {}!',
    'moikka, {}!',
    'салам, {}!'
]

def index(request):
    context = {
        'title': 'Do People Even Remember MySpace?',
        'greeting': list_of_hellos[randint(0, (len(list_of_hellos)-1))].format(request.user.username)
    }
    return render(request, 'story9/index.html', context)

def register(request):
    if request.user.is_authenticated:
        return redirect("story9:index")
    context = {
        "register_form": RegistrationForm,
        "title": 'Registration'
    }

    if request.method == "POST":
        register_candidate = RegistrationForm(request.POST)
        if register_candidate.is_valid():
            register_candidate.save()
            new_user = authenticate(
                username = register_candidate.cleaned_data['username'],
                password = register_candidate.cleaned_data['password1']
            )
            login(request, new_user)

            return redirect("story9:index")
        elif register_candidate.errors:
            context["error"] = "Please fill all forms correctly."
    return render(request, 'story9/register.html', context)



def logon(request):
    context = {
        "login_form": LoginForm, 
        "title": "Login"
    }
    if request.user.is_authenticated:
        return redirect("story9:index")

    if request.method == "POST":
        login_attempt = LoginForm(data = request.POST)
        if login_attempt.is_valid():
            user_account = login_attempt.get_user()
            login(request, user_account)
            return redirect("story9:index")
        elif login_attempt.errors:
            context["error"] = "Login data not valid."
    return render(request, 'story9/login.html', context)



def logoff(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("story9:index")
    
