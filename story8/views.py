from django.shortcuts import render
from django.http import JsonResponse
import urllib3
import json

# Create your views here.
def index(request):
    context = {
        'title': 'Library Exodus'
    }
    return render(request, 'story8/index.html', context)
    
def get_books(request):
    # thanks Mustafa! :D
    http = urllib3.PoolManager()
    request_arguments = "q="
    request_arguments += request.GET.get('q', '')
    request_arguments += (('&maxResults=' + request.GET['maxResults']) 
                         if (request.GET.get('maxResults', '') != '') else '')
    request_arguments += (('&startIndex=' + request.GET['startIndex']) 
                         if (request.GET.get('startIndex', '') != '') else '')
    response = http.request(
        "GET", "https://www.googleapis.com/books/v1/volumes?" + request_arguments)
    return JsonResponse(json.loads(response.data.decode('utf8')))