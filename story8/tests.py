from django.test import TestCase, Client
from django.urls import reverse
import json
# Create your tests here.

class story7Test(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story8:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"story8/index.html")

    def test_GET_API_page(self):
        response = self.client.get(reverse('story8:get_books') + '?q=Imagined%20Communities')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Imagined Communities', response.content.decode('utf-8'))
        
    def test_GET_API_with_variables(self):
        response = self.client.get(reverse('story8:get_books') + '?q=The%20Ego%20and%20Its%20Own&maxResults=20&startIndex=0')
        self.assertEqual(response.status_code, 200)
        self.assertIn('The Ego and Its Own', response.content.decode('utf-8'))
        result_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals(len(result_json['items']), 20)
        
        first_book = result_json['items'][0]
        response = self.client.get(reverse('story8:get_books') + '?q=The%20Ego%20and%20Its%20Own&maxResults=20&startIndex=10')
        result_json = json.loads(response.content.decode('utf-8'))
        second_book = result_json['items'][0]
        self.assertNotEqual(first_book, second_book)