// thanks again to Mustafa! :D
var noimage = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/200px-No_image_3x4.svg.png";
var noauthor = "No Author Found"
var currentIndex = 0
var currentBook = ""
var ajax_url = window.location.origin + "/story8/get_books/?q="
$(document).ready( function () {
    $( '#search-form' ).submit(function ( event ) {
        event.preventDefault();
        $( '#results' ).html("");
        currentBook = $( '#search-query' ).val();
        getResult();
        currentIndex += 10;
        $( '#view-more' ).remove();
        $( '#results ').after("<button id = 'view-more' onclick='viewMore()' class='btn btn-primary'>View more results</button>");
    })
})

function viewMore () {
    getResult();
    currentIndex += 10;
}

function getResult() {
    $.ajax({
        url: ajax_url + currentBook + "&maxResults=10&startIndex=" +  currentIndex,
        success: addResult
    });
}

function addResult( result ) {
    let json = result.items;
    let inner = "";
    for (let i = 0; i < json.length; i++) {
        let data = json[i].volumeInfo;
        inner += '<div class="card">';
        inner += '<a target="_blank" class="book-link" href="' + data.infoLink + '">';
        inner += '<div class="img-div"><img class="book-cover" src="' + (data.imageLinks === undefined ? noimage : data.imageLinks.thumbnail) + '"/></div>';
        inner += '<p class = "book-title">' + data.title + '</p>';
        inner += '<p class = "book-author">' + (data.authors === undefined ? noauthor : data.authors) + '</p>';
        inner += '</a>';
        inner += '<a target="_blank" href = "https://libgen.rs/search.php?req=' + data.title.replace(/ /g,'+').replace(/[^\w-]+/g,'') + '" class = "libgen-link">Find in libgen</a>';
        inner += '</div>';
    }
    
    $( '#results ').append( inner );
}

