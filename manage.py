#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""

# This Django-Gitlab-Heroku setup was done with immense help by Kak Okto <3
# from https://gist.github.com/lepiku/353b5811d81a35fc967a44326cfb9581
import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'story.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
