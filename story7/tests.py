from django.test import TestCase, Client
from django.urls import reverse
# Create your tests here.

class story7Test(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story7:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"story7/index.html")