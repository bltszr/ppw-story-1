from django.shortcuts import render

def index(request):
    return render(request, "profile_page/index.html")