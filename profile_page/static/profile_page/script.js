let root = document.documentElement;
// 1 is the default, white mode
var tate = 1;
function toggleTatemae() {
    if (tate == 0) {
        tate = 1;
        root.style.setProperty('--bg-color', "white");
        root.style.setProperty('--shadow-color', "black");
        document.getElementById("secret-button").innerHTML = "建前";
        document.getElementById("title-card").innerHTML = "よ～こそ！";
        document.getElementById("hobby").innerHTML = "Guitar, piano, digital painting, graphic design, manga";
        document.getElementById("interest").innerHTML = "Elementary music theory, functional programming, (human) linguistics, various areas of mathematics, historical theology esp. Christianity, Islam, and Buddhism; and whatever else that may momentarily catch my attention";
        document.getElementById("desc").innerHTML = "Hi there! People usually call me Dodo, others call me by a secret name. I'm a computer science student at Fasilkom UI, class of 2019. Unfortunately I don't do a lot of things so there's not much else for me to put here. よろしくお願いします！";
        document.getElementById("sotsu").innerHTML = "2019 - ????";
        document.getElementById("find-me").innerHTML = "Find me here!";
    } else {
        tate = 0;
        root.style.setProperty('--bg-color', "black");
        root.style.setProperty('--shadow-color', "white");
        document.getElementById("secret-button").innerHTML = "本音";
        document.getElementById("title-card").innerHTML = "メンゴ！";
        document.getElementById("hobby").innerHTML = "Being a moral nuisance towards the public conscience";
        document.getElementById("interest").innerHTML = "The proliferation of the thoughts and ideas presented by American mathematician Theodore J Kaczynski and French theorist Georges Sorel within the general circulation of the proletariat";
        document.getElementById("desc").innerHTML = "Hi there! People usually call me Dodo, but please don't call me! Ever! I'm a threat to public safety and should not be allowed to roam unattended! Please contact the nearest ██████████ and b̴͙̠͊̆̏ͨͬ́͊e̓́̈͝ ̺̦̕á̿̇w͎̠͔̼̫̯ͧ̾͘a̳͇̞͇̝̟͕̾ͫ͜r̞̮̞͕̘̙̤̂̇ȇ̻̹͇̬̝̇̀ ö̦̮̤̱̗̫͙́̔͐ͬͩͅf̰̹̱ͫͮ͞ ͭͧͨ̎̄͐̐́҉̣̭͈̼̱̪̜̺͞t̡̳̹̮̊̿̈́͆̓h̛͉̥̱͚̱̣ͯ́ͪ̎͐ḛ̷̡̬̣̬͇̺̋́ͩ͊͑̓͋̚ ̎̊̃͋ͯ̉̏̓͏̺̟̯̺͔̠̖̦f͎̩̲̩̺͒̆ͫâ̡̨̛͓̟c̯̤̳̼̫̲̊͂ͮ̃ͬ͗͆ͮ͠t̺͚̱̫ͬ͌ͬ͌ͫ̆͆͒ ̱͖̩̭̩͈̦̿̒ͮ̈́ͪ͆ͩͪ͠t̛̮̘̱̜̥̙͙̪͚̊̇h͎̩͇͉̱̋͛ͦͥ͊̿ͬ̽͢a͎̭ͦ͆ͫ̐̔̈́͊ͤ̀͝t͍͉͔̩͎͉ͨͩ P̴̢̲̩͓̟̭̬̳͓̟̀̾͗̃̄̃ͬ̐̏̅̐̒̊̃͑̔ͧ́̀͞ͅl̡̉̊̒̑͋ͭͩ͒ͨ̓̉̚͏̖̳̱̪̹̺̱͓̗̳̟̠͎̖̣͈͈͖̰ẽ̷̷̮͙̣̪̜̰̖ͪ̔ͩ̄͗ͪͦ̚͠a̸̧̞̰̺͎̬̗̯̣̼̩͇͈̫̠͔̭͇͖͛̈̃̄ͥͥ̆ͣ̎͜͠ş̶̢͍͕̤̼̲̩͇̰̹̥̤͙̻̦̺̖͗ͨ̌ͦͦ̄̌͊ͤ͑ͩ́̽̎̍̋̆ͮ̓ę̶̛̲̤͉͐͐̈̓̎̀ ̡̹͎̹̲͇̬̼̞ͥ̔ͪ͆͛̈̂͟I͐ͥ̾̔ͮ̃̂̂̽ͫ͊͒ͪ̓̿ͬͦ̊͡҉̣̮̫̞̯m̻̠͓̲̖ͫ͛̚͟m̢͙̥̪͕̦̣̯̮ͪ̒̅͐̓͊̓̋͐ͨ̆͑͛̂̀̽͟͠ē̲̤̗̙͔̣̗̦͓̫ͩ̓̿̀ͥ͡ͅd̛̝̠͇̫̱̺͈͎̼̬͍̯͖̤̩͍͒͂̌ͣ̅̔͠͡ͅỉͮ̉̏̾̑ͨ̓̄̿ͫ͜͜͠͏̤͚̤̠̮̟ͅa̡͑̈̽̀̍̋̍҉̶̢͓̝̟͚̳͙̪̯͖̺̦͈̟̠̱̗ͅt̴ͫͨͤ̅̑̄͋̂͏̵̬̮̭̝ͅė̵͎͙̰̦̮̣̣͚ͥ̀͂ͣ͒ͮͥ̔̀͠l̶̶̢̼̬͎̳̺͈͙̹͚̮ͩ̊͊ͧ͒ͪ̒́̉̈́̄̌͗̓͒́̚̚̕y̛͆ͬ̐ͣ̅͊͌͗̃̅ͬ̒̌̋̍̊̃͂́͜͠͏̲̠͍̤͖̹͎̝̘̞ͅͅ ͋ͧ̑ͫͨͪ̐͡͏̸̙̲͍̣̬̘̠͇̪̣̞̻̮͕͉̦̫͟Ŗ̢̛̻̬̫̙̳̠̺̫͓̗͚̦̣̟̫̃̿ͫ͆̏̇̉ͯͧ͘ȩͧ́ͩ̏̿̎͏̶̜̱̞̱̻͉͖̪͙͈͙̗̮͍̣̣̤̹͇͝c̑͋̈́͒̏̌͛̉͋̚͏̸̡̮̪̝̞͓͙͟͠í̵̟͇͉̰̟͎̮̩͔͈̪͚̲͇̪͖̩̇̉̇ͯͪ̅ͩͪ͢͞t̷̝̯̱̹͎ͨ̍͐̈̃̀ͥ̑ͫͮ̓̉̇̇͛͊͟͝e̛̩̠̹͓͖̺̞̼̿̄̉̿ͫ͊ͮ̎̽̀̀ Ǒ̴͔̥̤̻̙̜̙̰̔ͨ̆ͯ̄̃̑ͥ͆̋ͪͪ̍ͬ̋ͦͯ̀͜͟͠m̨̝͚̥ͣ̓̏̿ ̴͎̟̹̣̺̜̘̟̯͔̆̃̓ͮͥ̑̓͋ͪͨ̈́̈́ͫͣ͠S̎ͧͪ͑ͬͫ̽͂̓̀͏҉͟҉̦͔͓̥̗͍̗͍̥͓̬̼͎̱ḧ̴̡̧͓̯͉͙̣̪͈̲̦̤͖́̂̌ͩͪ̃̈̍̊͊̑͂͢͢ŗ̶̛ͥ̈͋̆̆ͩ̊ͤ̎̋̇͆͂ͣ̃̂͑͘͏̼̯̲̤̹̦̟̜͓i̷ͮͦ́̊ͯ͊̒́͐ͬ̽̑̅̾̀̚҉̩̥̥͍̪͇͙͖͞ ̷̦̣͉͙̻̠͉͉̯̥̞̜̫͚̻̝̯̱̟͐̀̽̌͆͆̀͗ͦ̀̀M̶̧̛͍̰̞͎̃̓͒̏ͮ͗̓͌̌̂ͭ͛á͒̂̂ͥ̊̄ͭ͂̓̔҉͉̻͈̲͍̣͇̖̤͙ḫ̸̳̯̺̘̤̜̥̺̬͇̪̩͙̣̜͕͎̉ͤ̈͗̐ͫ̀̾ͧ̄́̉͐̚͢ͅa̧̢̻̺͖̯̳̽ͥͯ͌͂̑̏́̈́̓ͤ́́͝k̶̢̫͖̮͓͚̖̹̙̙̜̬̽̎̊̇ͣ̽̍͞à̲͇̯̦̬̰̱͛͂ͩͦ̐́̓̃͢͡l̝̪͈͕̖͎̟͖̳͕͖̯̗̤̖̯̦͎͂ͧ̏ͩ͂̽̍̉̀͊͐ͩ̽̚͜͡a̴̾̒̈ͨ̏̈́̈ͮ͑ͪͥͣ͋͞͏͙̙͖ͅ ̷̡̧̲͇͍̘̳̩͔̟͉̖̣͖̍͆̒̐̊ͤ̍̒̋̔ͫͪ̾ͭ̌̕ͅY̠̖̪͇̥͎ͮ̽̉͋͊͂̎̎̉̃̍̎͂͌̏̚̚͝͞ā̴͈̗͔̦͈̑̂͑̇̽ͮ̈ͫ͒͠k͗ͩ͑̃̓̋̇ͪͣ͊ͩ͋̃͏̶̭͔͍͎͉̼͚͚͖̯͎̺̟̬̱͜ ̡̹̮̱̣͇ͥ̆̉̈́ͦ̇̉̿ͬͫ̏̓ͪ͗ͤ̓ͤ͗͟G̲̦͎̼͎̭̞̜͕͇̙̙̗̺̦̯ͣ͌́͐̽ͬ̏̏ͤ̊͒̽̃ͪ̽ͭ̇́̚̕͢͟͡y̒̎ͦͦ͋͋͗̚҉̴̙̮̭̗͇a̵̡̡̧̡̤͓̞̲͙̹̣͇̬͎̣͓͔ͫ̿̉ͬ̑ͨͣ̽ͬ̀͌ͮ ̻̣̻̹̫̯̼͉̝̣͉́̏ͣ̔͒ͣ̉͐̈ͭ̔͐̕Ḅ̶̜̝̭̭̲̯͇̥͔̤͍̬͙̹̅̀̿̉̈͗ͧͬ̈́̈́͑̈̀ȅ̸̛̤̗̘̗̻̯̹̔̓̈́͌͒̏̃ͯ̋̀ͪ̋̉͆̓̕͜͝ ͕͍̬̫̜͍̰͙̬̟̔̀̄ͦͮͩͭ͛͟͠T̴͐̈́̍ͨ́̐̂̂̆̄ͥ̓̀͞҉̫̖̦̻̜͓ä̇̽͒̇̈́ͩ̓ͭ̂ͣ͑̏̍̒͞͏̼̠̠̙̱̥̜̤͖͉̗ ̴̴̡̭̥͎̥̗̦͚̯̼͕̗̣̥͌̽͊͛̒ͮ͜Ľ̴͚̫̘͉̹̗̯̩̜͎̥ͭ̍ͯ̿ͣ͆̏ͥ̅̈̅̏͆̐̊͘i̾̇̔͂̋ͧ͊ͧ͑̕͠͏̧̨͕̞̤̹̠̺̺̰͉͍͖̰̟̼̥̯ ̷̺̘͔̖̰̻̥͙̼̰͔͎͚̖̅̆͌̏͂́̊̓̋̀ͧ͊̆̿͗̚͘ͅͅĤ̵̼̟̳̲̗͓̗̪͙̯̫̖̱̺̍̆ͫ̍̀ụ̢̡̘͙̺̠̯͗ͭͧͭ̒̌́̐̅ͣ̏͛ͣͦ͗͋̓ͭ̚͟͠m̸̵̨̺̗͕͇͎̯̝̯͑̄ͪ̍͢ ͊͌̈́͌̃҉̨̻͚̤̻͠Ḑ̮̫͉̩͔̼͖̗̹̌̂̑̋ͯ̂̊̆ͨͥ̐̀̚͢͞z̴̞͙̣̘̗̙̘̮̭͉̦͓̞̲͓̩̹̀̀͌̃ͫ̒̆͋̋ͤ̄͂͢͝ͅa̢͕̙͖̲̱͇̯̮̪͖̬͇̰̥͑ͦ̿̑̀̾̓̐̒͋͌̔̿̃͋ͭ̚̚ͅ";
        document.getElementById("sotsu").innerHTML = "2019 - 2027";
        document.getElementById("find-me").innerHTML = "Find me in your dreams! :^)";
    }
}