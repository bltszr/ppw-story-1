from django.test import TestCase, Client
from .models import Peserta, Kegiatan
from django.urls import reverse

# Create your tests here.
class Story6RoutingTests(TestCase):
    def setUp(self):
        self.TEST_name1 = "fooo"
        self.TEST_name2 = "bar"
        self.TEST_name3 = "baz"
        self.TEST_name4 = "bruh"
        self.TEST_name5 = ("A" * 257)
        self.TEST_name6 = "Mancing Bareng"
        self.client = Client()
        self.TEST_kegiatan = Kegiatan.objects.create(name = self.TEST_name4)
        self.TEST_kegiatan2 = Kegiatan.objects.create(name = self.TEST_name6)
        self.index_url = reverse('story6:index')
        self.add_kegiatan_url = reverse('story6:add_kegiatan')
        self.add_peserta_url = reverse('story6:add_peserta')
        self.TEST_peserta_to_delete = Peserta.objects.create(name = "del", 
                                      kegiatan = self.TEST_kegiatan)
        self.TEST_kegiatan_to_delete = Kegiatan.objects.create(name = "del")
        self.TEST_peserta_already_exists = Peserta.objects.create(name = self.TEST_name3, 
                                                   kegiatan = self.TEST_kegiatan)
    def test_GET_index(self):
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"story6/index.html")
    
    def test_GET_add_kegiatan(self):
        response = self.client.get(self.add_kegiatan_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"story6/add_kegiatan.html")
        
    def test_POST_add_kegiatan_violate(self):
        response = self.client.post(self.add_kegiatan_url, {"name": self.TEST_name5})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(self.TEST_name5, list(map(lambda x : str(x), Kegiatan.objects.all())))
        
    def test_POST_add_kegiatan(self):
        response = self.client.post(self.add_kegiatan_url, {"name": self.TEST_name2})
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.TEST_name2, list(map(lambda x : str(x), Kegiatan.objects.all())))

    def test_POST_add_peserta(self):
        # print(self.TEST_kegiatan.id)
        response = self.client.post(self.add_peserta_url, {"name": self.TEST_name2, "kegiatan": self.TEST_kegiatan.id})
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.TEST_name2, list(map(lambda x : str(x), self.TEST_kegiatan.participants())))

    def test_POST_add_peserta_ada_spasi(self):
        response = self.client.post(self.add_peserta_url, {"name": self.TEST_name2, "kegiatan": self.TEST_kegiatan2.id})
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.TEST_name2, list(map(lambda x : str(x), self.TEST_kegiatan2.participants())))

    def test_POST_delete_peserta(self):
        length = Peserta.objects.all().count()
        response = self.client.post(reverse('story6:delete_peserta', args=[self.TEST_peserta_to_delete.id]))
        self.assertEqual(Peserta.objects.all().count(), length-1)

    def test_POST_delete_kegiatan(self):
        length = Kegiatan.objects.all().count()
        response = self.client.post(reverse('story6:delete_kegiatan', args=[self.TEST_kegiatan_to_delete.id]))
        self.assertEqual(Kegiatan.objects.all().count(), length-1)

    def test_POST_add_peserta_already_exists(self):
        response = self.client.post(self.add_peserta_url, {"name": self.TEST_name3, "kegiatan": self.TEST_kegiatan.id})
        length = Peserta.objects.all().count()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Peserta.objects.all().count(), length)

    def test_POST_add_peserta_already_exists_somewhere_else(self):
        response = self.client.post(self.add_peserta_url, {"name": self.TEST_name2, "kegiatan": self.TEST_kegiatan.id})
        length = Peserta.objects.all().count()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Peserta.objects.all().count(), length)

    def test_POST_add_peserta_violate(self):
        response = self.client.post(self.add_peserta_url, {"name": self.TEST_name5, "kegiatan": self.TEST_kegiatan.id})
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(self.TEST_name5, list(map(lambda x : str(x), self.TEST_kegiatan.participants())))

    def test_GET_add_peserta(self):
        response = self.client.get(self.add_peserta_url)
        self.assertEqual(response.status_code, 302)
        
class Story6ModelTests(TestCase):
    def setUp(self):
        self.TEST_name1 = "foo"
        self.TEST_name2 = "bar"
        self.TEST_name3 = "baz"
        self.TEST_kegiatan = Kegiatan.objects.create(name = self.TEST_name1)
        self.TEST_kegiatan_count = Kegiatan.objects.all().count()
        self.TEST_peserta1 = Peserta.objects.create(name = self.TEST_name1, 
                                                   kegiatan = self.TEST_kegiatan)
        self.TEST_peserta_count = Peserta.objects.all().count()
        
        self.TEST_peserta2 = Peserta.objects.create(name = self.TEST_name2, 
                                                   kegiatan = self.TEST_kegiatan)
        self.TEST_peserta3 = Peserta.objects.create(name = self.TEST_name3, 
                                                   kegiatan = self.TEST_kegiatan)
        self.TEST_peserta_list = Peserta.objects.filter(kegiatan = self.TEST_kegiatan)

    def test_CREATE_kegiatan(self):
        
        self.assertEquals(str(self.TEST_kegiatan), self.TEST_name1)
        self.assertEqual(self.TEST_kegiatan_count, 1)

    def test_CREATE_peserta(self):
        
        self.assertEquals(str(self.TEST_peserta1), self.TEST_name1)
        self.assertEqual(self.TEST_peserta_count, 1)

    def test_ADD_peserta_to_kegiatan(self):
        self.assertIn(self.TEST_peserta1, self.TEST_peserta_list)
        self.assertIn(self.TEST_peserta2, self.TEST_peserta_list)
        self.assertIn(self.TEST_peserta3, self.TEST_peserta_list)