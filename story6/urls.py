from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_kegiatan/', views.add_kegiatan, name='add_kegiatan'),
    path('add_peserta/', views.add_peserta, name='add_peserta'),
    path('delete_peserta/<int:peserta_id>', views.delete_peserta, name='delete_peserta'),
    path('delete_kegiatan/<int:kegiatan_id>', views.delete_kegiatan, name='delete_kegiatan'),
]