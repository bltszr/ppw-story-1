from django import forms
from .models import Kegiatan, Peserta
from django.core.exceptions import ValidationError

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"

class PesertaForm(forms.Form):
    name = forms.CharField(label='Nama anda', max_length=256)
    kegiatan = forms.ModelChoiceField(queryset=Kegiatan.objects.all())
    
    def clean_name(self):
        name = self.cleaned_data['name']
        if Peserta.objects.filter(name=name).exists():
            raise ValidationError("Peserta already exists")
        return name
