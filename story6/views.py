from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm
from django.urls import reverse

# Create your views here.
def index(request):
    context = {
        'kegiatan_list': Kegiatan.objects.all()
    }
    return render(request, 'story6/index.html', context)
    
def add_kegiatan(request):
    if request.method == 'POST':
        add_kegiatan_form = KegiatanForm(request.POST)
        if add_kegiatan_form.is_valid():
            add_kegiatan_form.save()
            messages.add_message(request, messages.SUCCESS,
            "Kegiatan {} berhasil ditambahkan!".format(request.POST.get('name')))
            return redirect(reverse('story6:index'))
        else:
            messages.add_message(request, messages.ERROR,
            "Kegiatan {} TIDAK berhasil ditambahkan!".format(request.POST.get('name')))

    return render(request, 'story6/add_kegiatan.html')

def add_peserta(request):
    if request.method == 'POST':
        kegiatan = Kegiatan.objects.get(id = request.POST.get('kegiatan'))
        # print(kegiatan)
        nama_peserta = request.POST.get('name')
        add_peserta_form = PesertaForm(request.POST)
        if add_peserta_form.is_valid():
            existing_peserta, new_peserta = Peserta.objects.get_or_create(name = nama_peserta,
            defaults={'name': nama_peserta, 'kegiatan': kegiatan})
            if (existing_peserta not in Peserta.objects.all()) or new_peserta:
                existing_peserta.save()
                messages.add_message(request, messages.SUCCESS,
                "Peserta {} berhasil didaftarkan!".format(nama_peserta))
                # print("Peserta {} berhasil didaftarkan!".format(nama_peserta))
                return redirect(reverse('story6:index'))
        else:
            # print(add_peserta_form.errors)
            # print(add_peserta_form.kegiatan)
            messages.add_message(request, messages.ERROR,
            "Peserta {} TIDAK berhasil didaftarkan!".format(request.POST.get('name')))
            # print("Peserta {} TIDAK berhasil didaftarkan!".format(request.POST.get('name')))
    return redirect(reverse('story6:index'))

def delete_peserta(request, peserta_id):
    if request.method == 'POST':
        # print("Request is correct")
        peserta = Peserta.objects.get(id = peserta_id)
        # print("Peserta")
        peserta.delete()
        messages.add_message(request, messages.SUCCESS,
        "Peserta {} berhasil ditidakjadidaftarkan!".format(peserta))
    return redirect(reverse('story6:index'))
    
def delete_kegiatan(request, kegiatan_id):
    if request.method == 'POST':
        kegiatan = Kegiatan.objects.get(id = kegiatan_id)
        kegiatan.delete()
        messages.add_message(request, messages.SUCCESS,
        "Kegiatan {} sekarang sudah jadi wacana saja :(".format(kegiatan))
    return redirect(reverse('story6:index'))
    