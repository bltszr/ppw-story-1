from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    name = models.CharField(max_length = 256, unique=True)
    def __str__(self):
        return self.name
    def participants(self):
        return Peserta.objects.filter(kegiatan = self)
    class Meta:
        ordering = ['-name']

class Peserta(models.Model):
    name = models.CharField(max_length = 256, unique=True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.CASCADE)
    def __str__(self):
        return self.name
    class Meta:
        ordering = ['-name']