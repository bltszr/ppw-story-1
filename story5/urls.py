from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_matkul/', views.add_matkul, name='add_matkul'),
    path('detail_matkul/<slug:matkul_id>', views.detail_matkul, name='detail_matkul'),
    path('delete_matkul/<slug:matkul_id>', views.delete_matkul, name='delete_matkul')
]