from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from django.db import models

# Create your models here.

# Mata kuliah tersebut meliputi nama mata kuliah, dosen pengajar, 
# jumlah sks, deskripsi mata kuliah, semester tahun(contoh: Gasal 2019/2020) 
# dan ruang kelas dimana mata kuliah tersebut dilaksanakan.
class MataKuliah(models.Model):

    def validate_semester(candidate_string):
        try:
            candidate = candidate_string.split(' ')
            if len(candidate) == 2:
                if candidate[0].upper() == 'GASAL' or candidate[0].upper() == 'GENAP':
                    years = candidate[1].split('/')
                    if (int(years[1]) - int(years[0])) == 1:
                        return None
            raise ValidationError(
                _('%(candidate_string) is not a valid term year'),
                params={'candidate_string': candidate_string},
            )
        except:
            raise ValidationError(
                _('%(candidate_string) is not a valid term year'),
                params={'candidate_string': candidate_string},
            )
    nama_matkul = models.TextField(max_length=100);
    nama_dosen = models.TextField(max_length=100);
    jumlah_sks = models.IntegerField();
    deskripsi = models.TextField(max_length=2048);
    # this one will be validated using a custom one
    # because regex can't match for increments. This is 
    # just for illustration
    # semester_tahun = r'^((Gasal|Genap) {year}/{year+1})$'
    semester_tahun = models.CharField(max_length=15, validators=[validate_semester]);
    # ruangan = r'^((A[1-9]\.[0-9]{2})|((?P<bldg>[1-3])\.(?P=bldg)[0-9]{3})|(online))$'
    ruangan = models.CharField(max_length=16, validators=[
        RegexValidator(regex = 
            r'^((A[1-9]\.[0-9]{2})|((?P<bldg>[1-3])\.(?P=bldg)[0-9]{3})|(online))$'
        )]);
    # kode_matkul = r'^([A-Z]{4}[0-9]{6})$'
    kode_matkul = models.CharField(max_length=10, unique=True, validators=[
        RegexValidator(regex=
            r'^([A-Z]{4}[0-9]{6})$'
        )]);

    def __str__(self):
        return self.nama_matkul