from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import MataKuliahForm
from django.contrib import messages

def index(request):
    context = {'matkul_list': MataKuliah.objects.all().order_by('-kode_matkul')}
    return render(request, "story5/index.html", context)

def add_matkul(request):
    if request.method == "POST":
        add_request_form = MataKuliahForm(request.POST)
        if add_request_form.is_valid():
            add_request_form.save()
            messages.add_message(request, messages.SUCCESS, 
            "Mata kuliah {} berhasil ditambahkan!".format(request.POST["nama_matkul"]))
        else:
            messages.add_message(request, messages.ERROR, 
            "Mata kuliah {} TIDAK berhasil ditambahkan.".format(request.POST["nama_matkul"]))
            
    return render(request, "story5/add_matkul.html")

def detail_matkul(request, matkul_id):
    context = {'matkul': MataKuliah.objects.get(kode_matkul=matkul_id)}
    return render(request, "story5/detail_matkul.html", context)

def delete_matkul(request, matkul_id):
    if request.method == "POST":
        matkul = MataKuliah.objects.get(kode_matkul=matkul_id)
        matkul.delete()
        messages.add_message(request, messages.SUCCESS,
        "Mata kuliah {} sudah terhapus!".format(matkul.nama_matkul))
    return redirect("story5:index")
