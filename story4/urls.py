from django.urls import path
from . import views

app_name = 'profile_page'

urlpatterns = [
    path('', views.index, name='index'),
    path('blog_example/', views.blog_example, name='blog_example')
]