from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, "story4/index.html")
    
def blog_example(request):
    return render(request, "story4/blog_example.html")